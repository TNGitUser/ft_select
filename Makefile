# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lucmarti <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/05/15 12:27:00 by lucmarti          #+#    #+#              #
#    Updated: 2019/09/16 13:14:39 by lucmarti         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

Red=\033[0;31m
Green=\033[0;32m
Cyan=\033[0;36m
Yellow=\033[0;33m
End=\033[0m

NAME = ft_select

SRC_PATH = ./src

OBJ_PATH = ./obj
DEP_PATH = $(OBJ_PATH)
SRC_FILE := main.c exit.c cli.c read.c display.c signal.c update.c\
	read_aux.c tools.c bonus.c colors.c

OBJ_FILE = $(SRC_FILE:.c=.o)
DEP_FILE = $(SRC_FILE:.c=.d)

SRC = $(addprefix $(SRC_PATH)/,$(SRC_FILE))
OBJ = $(addprefix $(OBJ_PATH)/,$(OBJ_FILE))
DEP = $(addprefix $(DEP_PATH)/,$(DEP_FILE))

CFLAGS = -g -Wall -Wextra -Werror -I./inc
CPPFLAGS = -MMD -MP

LIB = ./libft/libft.a

UNAME_S = $(shell uname -s)
ifeq ($(UNAME_S),Linux)
	ECHO = echo -e
else
	ECHO = echo
endif

all: $(NAME)

$(NAME): $(OBJ) 
	@$(MAKE) -C ./libft/
	@gcc $(CFLAGS) $(LIBFLAGS) -l termcap -o $(NAME) $(OBJ) $(LIB)
	@$(ECHO) "Compilation of $(Cyan)$(NAME)$(End) :    $(Green)Done$(End)"

$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c ./inc/ft_select.h
	@mkdir -p $(OBJ_PATH) 2> /dev/null || true
	@gcc $(CFLAGS) $(CPPFLAGS) -o $@ -c $<
	@$(ECHO) "[${Green}Compiled${End}] : $<"

.PHONY: clean fclean re

clean:
	@rm -rf $(OBJ)
	@rm -rf $(DEP)
	@$(MAKE) -C ./libft/ clean
	@rmdir -p $(OBJ_PATH) 2> /dev/null || true
	@$(ECHO) "[$(Red)MAKEFILE$(End)] Removing objects"

fclean: clean
	@rm -rf $(NAME)
	@rm -rf $(LIB)
	@$(MAKE) -C ./libft/ fclean
	@$(ECHO) "[$(Red)MAKEFILE$(End)] Removing binaries"

re: fclean all
-include DEP
