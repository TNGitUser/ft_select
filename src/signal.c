/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   signal.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucmarti <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/10 16:04:53 by lucmarti          #+#    #+#             */
/*   Updated: 2019/09/16 10:09:46 by lucmarti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_select.h"

static t_env *g_env;

static void	sig_resize(int signum)
{
	(void)signum;
	set_display(g_env);
	if (g_env->epp == 0)
		g_env->epp = 1;
	g_env->bookmark.total = g_env->data_settings[1] / g_env->epp;
	if (g_env->bookmark.total == 0
			|| g_env->data_settings[1] % g_env->epp != 0)
		g_env->bookmark.total += 1;
	g_env->bookmark.total = g_env->bookmark.total == 0
		? 1 : g_env->bookmark.total;
	g_env->bookmark.cur = 0;
	g_env->current_item->is_hover = 0;
	g_env->current_item = g_env->collection;
	g_env->current_item->is_hover = 1;
	g_env->first_item = g_env->current_item;
	display_refresh(g_env);
}

static void	sig_tstp(int signum)
{
	char	cp[2];

	(void)signum;
	cp[0] = g_env->term_data.c_cc[VSUSP];
	cp[1] = 0;
	g_env->term_data.c_lflag |= (ICANON | ECHO);
	tcsetattr(STDIN_FILENO, TCSANOW, &(g_env->term_data));
	signal(SIGTSTP, SIG_DFL);
	ioctl(0, TIOCSTI, cp);
	ft_printf("\x1b[H\x1b[J");
}

static void	sig_reset(int signum)
{
	signal(SIGTSTP, sig_tstp);
	sig_resize(signum);
	(void)signum;
}

static void	sig_kill(int signum)
{
	(void)signum;
	tcsetattr(STDIN_FILENO, TCSANOW, &(g_env->term_data));
	ft_printf("\x1b[H\x1b[J");
	mexit(g_env);
}

void		signal_catch(t_env *env)
{
	g_env = env;
	if (signal(SIGWINCH, sig_resize) == SIG_ERR)
		error(env, "Signal 0 catch failed\n");
	if (signal(SIGCONT, sig_reset) == SIG_ERR)
		error(env, "Signal 1 catch failed\n");
	if (signal(SIGTSTP, sig_tstp) == SIG_ERR)
		error(env, "Signal 2 catch failed\n");
	if (signal(SIGINT, sig_kill) == SIG_ERR)
		error(env, "Signal 3 catch failed\n");
	if (signal(SIGTERM, sig_kill) == SIG_ERR)
		error(env, "Signal 4 catch failed\n");
	if (signal(SIGQUIT, sig_kill) == SIG_ERR)
		error(env, "Signal 5 catch failed\n");
	if (signal(SIGABRT, sig_kill) == SIG_ERR)
		error(env, "Signal 6 catch failed\n");
}
