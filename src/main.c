/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucmarti <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/22 10:23:52 by lucmarti          #+#    #+#             */
/*   Updated: 2019/09/16 12:57:16 by lucmarti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_select.h"

static void		init_environment(int ac, char **av, t_env *env)
{
	int				i;
	size_t			dim;

	env->collection = NULL;
	i = 1;
	dim = 1;
	while (i < ac)
	{
		if (ft_strlen(av[i]) > dim)
			dim = ft_strlen(av[i]);
		cli_add(av[i++], env);
	}
	env->current_item = ac > 1 ? env->collection : NULL;
	env->first_item = ac > 1 ? env->collection : NULL;
	if (env->current_item)
		env->current_item->is_hover = 1;
	env->data_settings[0] = dim;
	env->data_settings[1] = ac - 1;
	set_display(env);
	if (env->epp == 0)
		env->epp = 1;
	env->bookmark.total = env->data_settings[1] / env->epp + 1;
	env->bookmark.total = env->bookmark.total == 0 ? 1 : env->bookmark.total;
	env->bookmark.cur = 0;
	env->locked = 0;
}

static void		retrieve_selected(t_env *env)
{
	t_cli	*cur;
	int		started;

	if (env->collection == NULL)
		return ;
	cur = env->collection;
	started = 0;
	while (!cur->is_head || !started)
	{
		if (cur->is_head)
			started = 1;
		if (cur->is_selected)
		{
			ft_putstr_fd(cur->value, 1);
			if (!cur->next->is_head)
				ft_putstr_fd(" ", 1);
		}
		cur = cur->next;
	}
	ft_putstr_fd("\n", 1);
}

int				main(int ac, char **av)
{
	long	ret;
	char	buf[2];
	t_env	env;

	buf[1] = 0;
	init_environment(ac, av, &env);
	ret = 0;
	signal_catch(&env);
	while (1)
	{
		display_refresh(&env);
		if ((ret = process_key(&env)) == -1)
			break ;
		if (ret == 27 || ret == 4 || ret == 10)
			break ;
	}
	ft_printf("\x1b[H\x1b[J");
	if (ret == 10)
		retrieve_selected(&env);
	mexit(&env);
	return (0);
}
