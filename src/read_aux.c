/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_aux.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucmarti <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/11 15:27:34 by lucmarti          #+#    #+#             */
/*   Updated: 2019/09/16 13:09:45 by lucmarti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_select.h"

static void	move_ho_aux(t_env *env, int mod)
{
	if (mod == -1)
	{
		if (env->current_item == env->last_item)
			change_page(env, ARROW_DOWN);
		else
			env->current_item = env->current_item->next;
	}
	else
	{
		if (env->current_item == env->first_item)
			change_page(env, ARROW_UP);
		else
			env->current_item = env->current_item->prev;
	}
}

void		move_ho(t_env *env, int key)
{
	int steps;
	int	mod;

	steps = env->win.e_frame ? env->rows - 2 : env->rows;
	steps = key == ARROW_RIGHT ? steps : -steps;
	mod = steps > 0 ? -1 : 1;
	while (steps != 0)
	{
		env->current_item->is_hover = 0;
		move_ho_aux(env, mod);
		steps += mod;
	}
	env->current_item->is_hover = 1;
	display_refresh(env);
}

void		del_item(t_env *env)
{
	t_cli *nitem;

	nitem = env->current_item->next;
	cli_del(env->current_item, 1, env);
	env->current_item = nitem;
	set_display(env);
	env->bookmark.total = env->data_settings[1] / env->epp + 1;
	env->bookmark.total = env->bookmark.total == 0
		? 1 : env->bookmark.total;
	if (env->bookmark.cur > 0 && env->bookmark.cur >= env->bookmark.total)
		env->bookmark.cur -= 1;
	if (env->collection == NULL || env->data_settings[1] <= 0)
	{
		ft_printf("\x1b[H\x1b[J");
		mexit(env);
	}
	display_refresh(env);
}

int			key_handler(int key)
{
	int			i;
	static int	codex[7] = { 'A', 'B', 'C', 'D', '5', '6', '3'};
	static int	cypher[7] = { ARROW_UP, ARROW_DOWN, ARROW_RIGHT, ARROW_LEFT,
		PAGE_UP, PAGE_DOWN, DEL};

	i = 0;
	while (i < 7)
	{
		if (codex[i] == key)
			return (cypher[i]);
		++i;
	}
	return ('\x1b');
}

int			read_key(t_env *env)
{
	int		nread;
	char	c;
	char	seq[4];

	c = 0;
	seq[0] = 0;
	seq[1] = 0;
	nread = read(STDIN_FILENO, &c, 1);
	if (nread == -1)
		error(env, "Termcaps read failed\n");
	if (c == '\x1b')
	{
		if ((read(STDIN_FILENO, &seq[0], 1) == -1)
				|| (read(STDIN_FILENO, &seq[1], 1) == -1))
			return ('\x1b');
		if (!(seq[0] == '['))
			return ('\x1b');
		if (seq[1] >= '3' && seq[1] <= '6')
			read(STDIN_FILENO, &seq[0], 1);
		return (key_handler(seq[1]));
	}
	return (c);
}
