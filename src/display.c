/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucmarti <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/10 10:19:33 by lucmarti          #+#    #+#             */
/*   Updated: 2019/09/16 09:44:33 by lucmarti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_select.h"

void		display_value(t_cli *cli, int x, int y)
{
	char	*pref;
	char	*suf;

	suf = "\x1b[0m";
	pref = "";
	if (cli->is_selected && cli->is_hover)
		pref = "\x1b[4;7m";
	else if (cli->is_selected)
		pref = "\x1b[7m";
	else if (cli->is_hover)
		pref = "\x1b[4m";
	cli->pin.x = x;
	cli->pin.y = y;
	ft_printf("%s%s%s%s", cli->color, pref, cli->value, suf);
}

static void	display_limits(t_env *env)
{
	int x;
	int y;

	x = 0;
	while (x < env->rows)
	{
		ft_printf("\x1b[%i;%iH", x + 1, 1);
		if (x == env->rows)
			return ;
		y = 0;
		if (x == 0 || x == env->rows - 1)
			while (y < env->cols)
			{
				display_lchar(DFT);
				++y;
			}
		else
		{
			ft_printf("\x1b[%i;1H", x + 1);
			display_lchar(DFT);
			ft_printf("\x1b[%i;%iH", x + 1, env->cols);
			display_lchar(DFT);
		}
		++x;
	}
}

static void	display_data_col(t_env *env, int st_line, int st_col, int offset)
{
	t_cli	*cur;
	int		org;

	org = st_line;
	if (env->collection == NULL)
		error(env, "ft_select : Empty data collection\n");
	cur = gtfi(env->collection, env->bookmark.cur * env->epp);
	display_value(cur, st_line, st_col);
	env->last_item = cur;
	cur = cur->next;
	while (!cur->is_head && st_line < env->rows)
	{
		++st_line;
		if (st_line >= env->rows)
		{
			st_line = org;
			st_col += offset;
			if (env->cols - st_col < offset)
				break ;
		}
		ft_printf("\x1b[%i;%iH", st_line, st_col);
		display_value(cur, st_line, st_col);
		env->last_item = cur;
		cur = cur->next;
	}
}

static void	display_fetch_style(t_env *env, int st_x, int st_y)
{
	ft_printf("\x1b[%i;%iH", st_x, st_y);
	display_data_col(env, st_x, st_y, env->data_settings[0]
			+ env->win.offset);
	return ;
}

void		display_refresh(t_env *env)
{
	if (env->cols < env->data_settings[0])
		env->locked = 1;
	else
		env->locked = 0;
	ft_printf("\x1b[H\x1b[J");
	if (env->locked)
		return ;
	if (env->win.e_frame && !FANCY)
		display_limits(env);
	else if (env->win.e_frame && FANCY)
		display_fancy(env);
	display_fetch_style(env, env->win.st_x, env->win.st_y);
	if (env->win.e_info)
	{
		ft_printf("\x1b[%i;2H", env->rows);
		ft_printf("%i : %i", env->rows, env->cols);
		ft_printf("\x1b[%i;%iH", env->rows, env->cols - 20);
		ft_printf("EPP : %i | %i / %i", env->epp, env->bookmark.cur + 1
				, env->bookmark.total);
	}
	ft_printf("\x1b[%i;%iH", env->current_item->pin.x
			, env->current_item->pin.y);
}
