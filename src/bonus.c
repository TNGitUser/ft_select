/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucmarti <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/10 10:19:33 by lucmarti          #+#    #+#             */
/*   Updated: 2019/09/12 11:37:51 by lucmarti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_select.h"

void		display_lchar(char *toprint)
{
	if (toprint == NULL || ft_strlen(toprint) == 0
			|| (ft_strlen(toprint) > 1 && ft_isprint(toprint[0])))
		toprint = "~";
	write(0, toprint, ft_strlen(toprint));
}

static void	display_vert(t_env *env, int x, char *fchar)
{
	fchar = VERT;
	ft_printf("\x1b[%i;1H", x + 1);
	display_lchar(fchar);
	ft_printf("\x1b[%i;%iH", x + 1, env->cols);
	display_lchar(fchar);
}

void		display_fancy(t_env *env)
{
	int		x;
	int		y;
	char	*fchar;

	x = 0;
	while (x < env->rows)
	{
		ft_printf("\x1b[%i;%iH", x + 1, 1);
		if (x == env->rows)
			return ;
		y = 0;
		if (x == 0 || x == env->rows - 1)
			while (y < env->cols && (fchar = HORI) == HORI)
			{
				if (x == 0 && (y == 0 || y == env->cols - 1))
					fchar = y == 0 ? TL : TR;
				else if (x == env->rows - 1 && (y == 0 || y == env->cols - 1))
					fchar = y == 0 ? BL : BR;
				display_lchar(fchar);
				++y;
			}
		else
			display_vert(env, x, fchar);
		++x;
	}
}
