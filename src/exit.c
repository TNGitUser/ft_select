/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucmarti <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/22 10:50:34 by lucmarti          #+#    #+#             */
/*   Updated: 2019/09/16 13:17:12 by lucmarti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_select.h"

void	error(t_env *env, char *msg)
{
	cli_free(env);
	tcsetattr(STDIN_FILENO, TCSAFLUSH, &(env->term_data));
	ft_printf("\x1b[H\x1b[J");
	if (msg)
		ft_putstr_fd(msg, 2);
	exit(1);
}

void	mexit(t_env *env)
{
	cli_free(env);
	tcsetattr(STDIN_FILENO, TCSAFLUSH, &(env->term_data));
	ft_printf("\x1b[J");
	exit(0);
}
