/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   update.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucmarti <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/11 15:11:32 by lucmarti          #+#    #+#             */
/*   Updated: 2019/09/11 15:22:27 by lucmarti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

static void		enable_raw_mode(t_env *env)
{
	struct termios raw;

	tcgetattr(STDIN_FILENO, &(env->term_data));
	raw = env->term_data;
	raw.c_lflag &= ~(ECHO | ICANON);
	raw.c_cc[VMIN] = 0;
	raw.c_cc[VTIME] = 1;
	tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw);
}

static void		init_term(t_env *env)
{
	char	*term_type;

	term_type = getenv("TERM");
	if (term_type == NULL)
		term_type = "xterm-256color";
	if (!term_type || tgetent(NULL, term_type) <= 0)
		error(env, "Incorrect terminal or unknown terminal");
	enable_raw_mode(env);
}

void			set_display(t_env *env)
{
	t_display	*dp;
	int			var;

	init_term(env);
	dp = &env->win;
	dp->e_frame = 0;
	dp->offset = 0;
	env->rows = tgetnum("li");
	env->cols = tgetnum("co");
	if (env->rows >= 3 && env->cols >= env->data_settings[0] + 7)
	{
		dp->e_frame = 1;
		dp->offset = 4;
		dp->st_x = 2;
		dp->st_y = 3;
	}
	else
	{
		dp->st_x = 1;
		dp->st_y = 1;
	}
	dp->e_info = (dp->e_frame && env->cols > 10) ? 1 : 0;
	var = ((env->cols - dp->st_y) / (env->data_settings[0] + dp->offset));
	env->epp = (env->rows - dp->st_x) * (var > 0 ? var : 1);
}
