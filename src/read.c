/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucmarti <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/10 09:59:22 by lucmarti          #+#    #+#             */
/*   Updated: 2019/09/16 10:11:03 by lucmarti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_select.h"

static void	move_to(t_env *env)
{
	t_cli *cur;

	cur = env->current_item;
	ft_printf("\x1b[%i;%iH", cur->pin.x, cur->pin.y);
	cur->is_hover = 1;
	display_value(cur, cur->pin.x, cur->pin.y);
	ft_printf("\x1b[%i;%iH", cur->pin.x, cur->pin.y);
}

static void	select_to(t_env *env)
{
	env->current_item->is_selected = !env->current_item->is_selected;
	move_to(env);
}

void		change_page(t_env *env, int key)
{
	ft_printf("\x1b[H\x1b[J");
	if (key == ARROW_UP)
	{
		env->last_item = env->first_item->prev;
		env->current_item = env->last_item;
		env->bookmark.cur -= 1;
		if (env->bookmark.cur < 0)
			env->bookmark.cur = env->bookmark.total - 1;
		env->first_item = gtfi(env->collection, env->epp * (env->bookmark.cur));
	}
	else if (key == ARROW_DOWN)
	{
		env->current_item = env->current_item->next;
		env->bookmark.cur += 1;
		if (env->bookmark.cur >= env->bookmark.total)
			env->bookmark.cur = 0;
		env->first_item = gtfi(env->collection, env->epp * env->bookmark.cur);
	}
	display_refresh(env);
}

static void	arrow_action(t_env *env, int key)
{
	env->current_item->is_hover = 0;
	display_value(env->current_item, env->current_item->pin.x,
			env->current_item->pin.y);
	if ((key == ARROW_UP && env->current_item == env->first_item)
			|| (key == ARROW_DOWN
				&& env->current_item == env->last_item))
		change_page(env, key);
	else if (key == ARROW_UP)
		env->current_item = env->current_item->prev;
	else if (key == ARROW_DOWN)
		env->current_item = env->current_item->next;
	ft_printf("\x1b[%i;1H", env->rows);
	move_to(env);
}

/*
**	BACKSPACE = 127
*/

int			process_key(t_env *env)
{
	int	key;

	key = 0;
	while (1)
	{
		key = read_key(env);
		if (!env->locked && (key == ARROW_UP || key == ARROW_DOWN))
			arrow_action(env, key);
		if (!env->locked && (key == ARROW_LEFT || key == ARROW_RIGHT))
			move_ho(env, key);
		else if (!env->locked && key == 32)
			select_to(env);
		else if (!env->locked && (key == 127 || key == DEL))
			del_item(env);
		if (key == 27 || key == 4 || (key == 10 && !env->locked))
			break ;
	}
	return (key);
}
