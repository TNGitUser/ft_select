/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucmarti <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/11 15:14:47 by lucmarti          #+#    #+#             */
/*   Updated: 2019/09/11 15:23:16 by lucmarti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_select.h"

t_cli	*gtfi(t_cli *head, int offset)
{
	int		i;
	t_cli	*item;

	i = 0;
	item = head;
	while (i < offset)
	{
		++i;
		item = item->next;
	}
	return (item);
}
