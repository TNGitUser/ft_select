/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cli.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucmarti <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/22 10:57:54 by lucmarti          #+#    #+#             */
/*   Updated: 2019/09/12 15:12:17 by lucmarti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_select.h"

static t_cli	*cli_create(char *value, t_cli *next, t_cli *prev, t_env *env)
{
	t_cli *node;

	if (!(node = malloc(sizeof(t_cli))))
		error(env, "Circular list, node allocation failed");
	node->is_selected = 0;
	node->is_hover = 0;
	node->color = "\x1b[0m";
	node->pin.x = 0;
	node->pin.y = 0;
	node->is_head = (next == NULL && prev == NULL) ? 1 : 0;
	node->value = ft_strdup(value);
	node->color = get_color(node);
	node->prev = prev ? prev : node;
	node->next = next ? next : node;
	if (prev)
		prev->next = node;
	if (next)
		next->prev = node;
	return (node);
}

void			cli_add(char *value, t_env *env)
{
	t_cli *prev;
	t_cli *node;

	if (env->collection == NULL)
		env->collection = cli_create(value, NULL, NULL, env);
	else
	{
		prev = env->collection;
		while (prev->next != env->collection)
			prev = prev->next;
		node = cli_create(value, prev->next, prev, env);
	}
}

void			cli_del(t_cli *node, int relink, t_env *env)
{
	if (!node)
		return ;
	if (relink)
	{
		if (node->prev == node)
			env->collection = NULL;
		else if (node->is_head)
		{
			node->next->is_head = 1;
			env->collection = node->next;
			node->prev->next = node->next;
			node->next->prev = node->prev;
		}
		else
		{
			node->prev->next = node->next;
			node->next->prev = node->prev;
		}
	}
	env->data_settings[1] -= 1;
	ft_memdel((void **)&(node->value));
	ft_memdel((void **)&node);
}

void			cli_free(t_env *env)
{
	t_cli *head;
	t_cli *node;

	head = env->collection;
	if (head == NULL)
		return ;
	node = head->next;
	while (node != head)
	{
		node = node->next;
		cli_del(node->prev, 0, env);
	}
	cli_del(head, 0, env);
	env->collection = NULL;
}
