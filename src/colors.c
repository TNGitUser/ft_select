/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   colors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucmarti <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/12 14:14:16 by lucmarti          #+#    #+#             */
/*   Updated: 2019/09/16 10:13:56 by lucmarti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_select.h"

static int	is_ended(char *str, char *end)
{
	size_t	i;
	size_t	j;
	int		len;

	len = ft_strlen(str);
	if (ft_strlen(end) >= (size_t)len)
		return (0);
	len--;
	i = len;
	j = ft_strlen(end) - 1;
	while (len >= 0)
	{
		if (str[i] != end[j])
			return (0);
		if (len == 0 || j == 0)
			break ;
		--len;
		--i;
		--j;
	}
	return (1);
}

static char	*isof_type(char *name, struct stat buf)
{
	char *cname;

	cname = ft_strrchr(name, '/') ? ft_strrchr(name, '/') + 1 : name;
	if (cname[0] == '.' && is_ended(cname, ".c"))
		return (C_HCFI);
	else if (is_ended(cname, ".c"))
		return (C_CFI);
	else if (cname[0] == '.' && is_ended(cname, ".h"))
		return (C_HHFI);
	else if (is_ended(cname, ".h"))
		return (C_HFI);
	else if (ft_strequ(cname, "Makefile"))
		return (C_MFI);
	else if (cname[0] == '.' && (S_ISDIR(buf.st_mode)))
		return (C_HDIR);
	else if ((S_ISDIR(buf.st_mode)))
		return (C_DIR);
	else if (cname[0] == '.' && access(name, X_OK) == 0)
		return (C_HEXE);
	else if (access(name, X_OK) == 0)
		return (C_EXE);
	else if (cname[0] == '.' && !(S_ISLNK(buf.st_mode)
				&& !(S_ISFIFO(buf.st_mode))))
		return (C_HID);
	return (NULL);
}

char		*get_color(t_cli *node)
{
	struct stat buf;
	char		*ret;

	if (!COLOR)
		return (NORMAL);
	if (lstat(node->value, &buf) == -1)
		return (NORMAL);
	if ((ret = isof_type(node->value, buf)) != NULL)
		return (ret);
	else if (S_ISFIFO(buf.st_mode))
		return (C_FIFO);
	else if (S_ISLNK(buf.st_mode))
		return (C_LN);
	return (NORMAL);
}
