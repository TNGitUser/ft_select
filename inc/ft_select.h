/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_select.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucmarti <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/22 10:27:47 by lucmarti          #+#    #+#             */
/*   Updated: 2019/09/16 11:44:33 by lucmarti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SELECT_H
# define FT_SELECT_H

# include <stdlib.h>
# include <unistd.h>
# include <fcntl.h>
# include <signal.h>

# include <termcap.h>
# include <term.h>
# include <termios.h>
# include <sys/ioctl.h>

# include <sys/stat.h>

# include "libft.h"

# define FANCY 0

# define TR "┓"
# define TL "┏"
# define VERT "┃"
# define HORI "━"
# define BL "┗"
# define BR "┛"

# define DFT "~"

# define COLOR 0

# define C_DIR "\x1b[1;36m"
# define C_HDIR "\x1b[2;36m"
# define C_FIFO "\x1b[1;35m"
# define C_LN "\x1b[0;35m"
# define C_EXE "\x1b[1;31m"
# define C_HEXE "\x1b[2;31m"
# define C_HID "\x1b[2;37m"
# define C_CFI "\x1b[38;5;042m"
# define C_HCFI "\x1b[38;5;042;2m"
# define C_HFI "\x1b[38;5;039m"
# define C_HHFI "\x1b[38;5;039;2m"
# define C_MFI "\x1b[38;5;195m"

# define NORMAL "\x1b[0m"

enum			e_keys
{
	ARROW_LEFT = 1000,
	ARROW_RIGHT,
	ARROW_UP,
	ARROW_DOWN,
	PAGE_UP,
	PAGE_DOWN,
	DEL,
	BACKSPACE
};

typedef struct	s_cli
{
	t_vector2		pin;
	int				is_head;
	int				is_selected;
	int				is_hover;
	char			*value;
	char			*color;
	struct s_cli	*next;
	struct s_cli	*prev;
}				t_cli;

typedef struct	s_bookmark
{
	int			total;
	int			cur;
}				t_bookmark;

/*
**	Type of display :
**		- NONE : case where window is too tiny
**		- MINI : case where window is allowing at least the longest value to be
**				printed even if that's all present
**		- FULL : case where the display can be used in full extent
*/
typedef struct	s_display
{
	int			type;
	int			st_x;
	int			st_y;
	int			offset;
	int			e_frame;
	int			e_info;
}				t_display;

/*
**	data_settings correspond to the following settings :
**		- longest string (used to determine if column is big enough)
**		- number of element (used to determine if line is big enough)
*/

typedef struct	s_env
{
	int				locked;
	int				rows;
	int				cols;
	t_display		win;
	t_cli			*collection;
	t_cli			*current_item;
	t_cli			*first_item;
	t_cli			*last_item;
	t_bookmark		bookmark;
	int				old_epp;
	int				epp;
	int				data_settings[2];
	struct termios	term_data;
}				t_env;

void			set_display(t_env *env);

void			error(t_env *env, char *msg);
void			mexit(t_env *env);

void			cli_add(char *value, t_env *env);
void			cli_del(t_cli *node, int relink, t_env *env);
void			cli_free(t_env *env);

void			move_ho(t_env *env, int key);
void			del_item(t_env *env);
int				key_handler(int key);
int				read_key(t_env *env);

void			change_page(t_env *env, int key);
int				process_key(t_env *env);

void			display_value(t_cli *cli, int x, int y);
void			display_refresh(t_env *env);

void			signal_catch(t_env *env);

t_cli			*gtfi(t_cli *head, int offset);

void			display_lchar(char *fchar);
void			display_fancy(t_env *env);

char			*get_color(t_cli *node);

void			debug_show_cli(t_env *env);
void			debug_show_env(t_env *env);
#endif
